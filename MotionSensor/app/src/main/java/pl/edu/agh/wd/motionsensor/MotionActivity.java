package pl.edu.agh.wd.motionsensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

public class MotionActivity extends AppCompatActivity implements SensorEventListener {

    protected SensorManager sensorService;

    protected Sensor acceleratorSensor;

    private float x = 0;
    private float y = 0;
    private float z = 0;

    private TextView mXTextView;
    private TextView mYTextView;
    private TextView mZTextView;

    private long curTime;

    private long mLastUpdate = 0;

    private static float kFilteringFactor = 0.4f;
    private static float accel[] = new float[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motion);

        sensorService = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        acceleratorSensor = sensorService.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorService.registerListener(this, acceleratorSensor, SensorManager.SENSOR_DELAY_NORMAL);

        mXTextView = (TextView) findViewById(R.id.x_Text);
        mYTextView = (TextView) findViewById(R.id.y_text);
        mZTextView = (TextView) findViewById(R.id.z_text);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor mySensor = event.sensor;
        curTime = System.currentTimeMillis();



        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER && (curTime - mLastUpdate) > 100) {
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            accel[0] = x * kFilteringFactor + accel[0] * (1.0f - kFilteringFactor);
            accel[1] = y * kFilteringFactor + accel[1] * (1.0f - kFilteringFactor);
            accel[2] = z * kFilteringFactor + accel[2] * (1.0f - kFilteringFactor);
            x -= accel[0];
            y -= accel[1];
            z -= accel[2];

            long curTime = System.currentTimeMillis();

            long diffTime = (curTime - mLastUpdate);
            mLastUpdate = curTime;
            updateTextFields(x, y, z);
            sendDataUpdate(x, y, z);
        }
    }

    private void sendDataUpdate(float x, float y, float z) {


        new UpdateTask().execute(x, y, z);
    }

    private void updateTextFields(float x, float y, float z) {
        mXTextView.setText(String.valueOf(x));
        mYTextView.setText(String.valueOf(y));
        mZTextView.setText(String.valueOf(z));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    protected void onPause() {
        super.onPause();
        sensorService.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        sensorService.registerListener(this, acceleratorSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private class UpdateTask extends AsyncTask<Float, Void, Void> {


        @Override
        protected Void doInBackground(Float... params) {
            HttpClient httpclient = new DefaultHttpClient();
            String value = "http://192.168.1.115:8080/wd/motion/update" + "/" + String.valueOf(x)
                    + "/" + String.valueOf(y)
                    + "/" + String.valueOf(z);
            HttpPost httppost = new HttpPost(value);
            try {
                httpclient.execute(httppost);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
