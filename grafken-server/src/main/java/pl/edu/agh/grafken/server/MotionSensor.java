package pl.edu.agh.grafken.server;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("motion")
public class MotionSensor {
	
	@POST
	@Path("update/{x}/{y}/{z}")
	public void updateAcceleratorInfo(@PathParam("x")
	String x, @PathParam("y") String y,
	@PathParam("z") String z){
		System.out.println("X: " + x);
		System.out.println("Y: " + y);
		System.out.println("Z: " + z);
	}
}
